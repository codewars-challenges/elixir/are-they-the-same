defmodule Aretheythesame do
  def comp(nil, nil), do: true
  def comp(_, nil), do: false
  def comp(nil, _), do: false
  def comp([], []), do: true
  def comp(_, []), do: false
  def comp([], _), do: false
  def comp(a, b) do
    n = a |> Enum.map(&(&1 * &1))
    n -- b == []
  end
end